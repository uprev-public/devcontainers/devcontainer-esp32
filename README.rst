README 
======

This is a development container for developing ESP32 projects. It includes all of the dependencies for ESP32 development and `mrtutils <https://mrt.readthedocs.io/en/latest/>`_

Once the container is open, run:

.. code:: bash 

     ./opt/esp/idf/export.sh 


Issues 
------

There is an issue where you may get an error about unsupported locale settings. This can be fixed with the following:

.. code:: bash 

    export LC_ALL="en_US.UTF-8"
    export LC_CTYPE="en_US.UTF-8"
    dpkg-reconfigure locales



Args
----

IDF Version
~~~~~~~~~~~
To build image for a branch or tag of IDF:

.. code-block:: bash 

    build . --build-arg IDF_CLONE_BRANCH_OR_TAG=name

Or from a specific commit:

.. code-block:: bash 

    build . --build-arg IDF_CHECKOUT_REF=commit-id